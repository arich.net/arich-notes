# Logical Volumes Encryption

## Table of Content

---
* [1 Table of Content](#table-of-content)
* [2 Setting up LVM partitions – Ubuntu 14](#setting-up-lvm-partitions--ubuntu-14)
* [3 Setting up the encrypted LVM](#setting-up-the-encrypted-lvm)
* [4 Configure on Boot Encrypted LVM partitions](#configure-on-boot-encrypted-lvm-partitions)
* [5 Operation tasks with Encrypted LVM](#operation-tasks-with-encrypted-lvm)
    * [5.1 Resize encrypted LVM](#resize-encrypted-lvm)
    * [5.2 Decrypt LVM from a USB stick](#decrypt-lvm-from-a-usb-stick)
---

## Setting up LVM partitions – Ubuntu 14
Logical volumes allow us to use partitions and disks in a more flexible way like adding storage physical media to volume groups, which if required will let us increase or decrease linux partitions and file systems.

In this case we will add a new disk to a server and configure it on LVM, after we will configure encryption to this LVM in order to protect information in rest, we will be creating and encrypting logical volume groups for /home, /tmp and swap. Also in order to make this available on boot we will automate it on boot to get the key information from a USB stick and a Master Password.

---
**Note1**: All the following configuration will be done as a root user, it can be done also by adding the prefix “sudo”

---

* **Install LVM packages on the system**

```console
root@server:~# apt-get update
root@server:~# apt-get install lvm2
```

* **Add a disk to the system**

This is something that can be done for instance with VirtualBox.

![Add disk in VirtualBox](images/AddDiskVirtualBox.png "Add disk in Virtualbox")

We can check that the kernel recognized the new disk using `dmesg`

```console
root@server:~# dmesg | grep sdb
[ 1.954468] sd 3:0:0:0: [sdb] 8388608 512-byte logical blocks: (4.29 GB/4.00 GiB)
[ 1.955492] sd 3:0:0:0: [sdb] Write Protect is off
[ 1.955980] sd 3:0:0:0: [sdb] Mode Sense: 00 3a 00 00
[ 1.956619] sd 3:0:0:0: [sdb] Write cache: enabled, read cache: enabled, doesn't support DPO or FUA
[ 1.958773] sdb: unknown partition table
[ 1.959531] sd 3:0:0:0: [sdb] Attached SCSI disk
```

* **Format the new disk as LVM partition**

```console
root@server:~# fdisk /dev/sdb
Command (m for help): n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): p
Partition number (1-4, default 1):
Using default value 1
First sector (2048-8388607, default 2048):
Using default value 2048
Last sector, +sectors or +size{K,M,G} (2048-8388607, default 8388607):
Using default value 8388607
Command (m for help): t
Hex code (type L to list codes): 8e
Changed system type of partition 1 to 8e (Linux LVM)
 
Command (m for help): w
The partition table has been altered!
 
Calling ioctl() to re-read partition table.
Syncing disks.
```

* **Configure the new LVM structure**

    * Physical Volume
    * Volume Group
    * Logical Volume (This one will be created with 1GB size)

```console
root@server:~# pvcreate /dev/sdb1
  Physical volume "/dev/sdb1" successfully created
root@server:~# vgcreate NEW-VG /dev/sdb1
  Volume group "NEW-VG" successfully created
root@server:~# lvcreate -L 1GB -n "Home-LVM" NEW-VG
  Logical volume "Home-LVM" created
```

## Setting up the encrypted LVM

At this point we have the Logical Volume created for Home, but we want to encrypt itand configure it to mount it during boot of the Operating System.

* **Install cryptsetup utility**

```console
root@server:~# apt-get install cryptsetup
```

* **Write random data to the created LVM**

```console
root@server:~# badblocks -c 10240 -s -w -t random -v /dev/mapper/
```

* **Create a random key of 256 bits**
(Press the shift key and the space bar a few times to introduce some entropy into the system.)

```console
root@server:~# mkdir cryptokeys
root@server:~# dd if=/dev/random of=/root/cryptokeys/home-key bs=1 count=256
```

* **List supported ciphers by your system – check also the performance of each one**

```console
root@server:~# cryptsetup benchmark
# Tests are approximate using memory only (no storage IO).
PBKDF2-sha1       879677 iterations per second
PBKDF2-sha256     579964 iterations per second
PBKDF2-sha512     322837 iterations per second
PBKDF2-ripemd160  526393 iterations per second
PBKDF2-whirlpool  195047 iterations per second
#  Algorithm | Key |  Encryption |  Decryption
     aes-cbc   128b   515.1 MiB/s  1814.6 MiB/s
 serpent-cbc   128b    52.5 MiB/s   339.1 MiB/s
 twofish-cbc   128b   125.2 MiB/s   222.9 MiB/s
     aes-cbc   256b   338.6 MiB/s  1447.7 MiB/s
 serpent-cbc   256b    63.7 MiB/s   313.9 MiB/s
 twofish-cbc   256b   123.0 MiB/s   225.6 MiB/s
     aes-xts   256b  1170.4 MiB/s  1167.7 MiB/s
 serpent-xts   256b   287.8 MiB/s   316.5 MiB/s
 twofish-xts   256b   211.7 MiB/s   224.4 MiB/s
     aes-xts   512b   927.0 MiB/s   922.6 MiB/s
 serpent-xts   512b   326.9 MiB/s   324.5 MiB/s
 twofish-xts   512b   207.3 MiB/s   208.9 MiB/s
```

* **Prepare the encrypted device by using luksFormat**

```console
root@server:~# cryptsetup --verbose -y \
                          --cipher aes-cbc-essiv:sha256 \
                          --key-size 256 \
                          luksFormat /dev/NEW-VG/Home-LVM /root/cryptokeys/home-key
 
WARNING!
========
This will overwrite data on /dev/NEW-VG/Home-LVM irrevocably.
 
Are you sure? (Type uppercase yes): YES
Command successful.
```

* **Open formatted LUKS device**

```console
root@server:~# cryptsetup --key-file /root/cryptokeys/home-key \
                          luksOpen /dev/NEW-VG/Home-LVM Encrypted-Home-LVM
root@server:~# ls -l /dev/mapper/ | grep Encrypted
lrwxrwxrwx 1 root root       7 Mar 18 05:56 Encrypted-Home-LVM -&gt; ../dm-1
```

* **Add a passphrase to LUKS partition, to recover it in case the key is lost**

```console
root@server:~# cryptsetup --key-file=/root/cryptokeys/home-key luksAddKey /dev/NEW-VG/Home-LVM
Enter new passphrase for key slot:
Verify passphrase:
```

## Configure on Boot Encrypted LVM partitions

Now that we have setup the encrypted LUKS partition, we need to configure the operating system to mount it on boot.

* **Create a script to export to stdout the key.** In this case the script is simple, later I will share a script done to export the key from a USB stick

```bash
#!/bin/bash
# Ariel Vasquez v1.0
# Add the driver to recognize the usb storage

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
KEY=/root/cryptokeys/home-key
cat $KEY
```

* **Configure `/etc/crypttab`**. In this case we include two lines, the first one will look for the key using the script, and in the case of failure the second will ask for the passphrase on boot.

```console
# 
Encrypted-Home-LVM   /dev/NEW-VG/Home-LVM   none	luks,keyscript=/root/cryptokeys/key.sh,retry=1
Encrypted-Home-LVM   /dev/NEW-VG/Home-LVM   none	luks,retry=1
```

* **Configure `/etc/fstab`** to mount the Encrypted LVM on /home

```console
root@server:~# cat /etc/fstab | grep Encrypted
# Encrypted partition
/dev/mapper/Encrypted-Home-LVM   /home   ext4   errors=remount-ro 0  
```

## Operation tasks with Encrypted LVM

### Resize encrypted LVM

One good reason to use Encrypted LVM is the ability to dynamically allocate more space or even reduce it, so it can dynamically grow/shrink as per your requirements. For encrypted LVM it should be the same, so following steps explain how to do it.

* **Status of /home filesystem before operation**

```console
root@server:~# df -h | grep home
/dev/mapper/Encrypted-Home-LVM  990M  1.3M  922M   1% /home
```

* **Increase physical disk size**, can be achieved by increasing a VM disk size, or by adding a new disk and then attach it to the Volume Group. After attaching or increasing a disk we need to rescan the bus, this can be achieved by executing the following command.

```console
root@server:~# echo "- - -" &gt; /sys/class/scsi_host/host2/scan
```

* **Verify there are enough space on the Volume Group the LVM is located**

```console
root@server:~# vgdisplay
  --- Volume group ---
  VG Name               Volume_Group_Name
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  12
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                4
  Open LV               4
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               148.47 GiB
  PE Size               4.00 MiB
  Total PE              38009
  Alloc PE / Size       30208 / 118.00 GiB
  Free  PE / Size       7801 / 30.47 GiB
  VG UUID               P30G9l-jlJH-RdTT-M2aS-http-5gXE-0UJjFj
```

* **Increase the Logical Volume with the size required**

```console
root@server:~# lvresize -L +1GB /dev/NEW-VG/Home-LVM
  Extending logical volume Home-LVM to 2.00 GiB
  Logical volume Home-LVM successfully resized
```

* **Resize the encrypted partition**

```console
root@server:~# cryptsetup resize /dev/mapper/Encrypted-Home-LVM
```

* **Resize the filesystem**

```console
root@server:~# resize2fs /dev/mapper/Encrypted-Home-LVM
resize2fs 1.42.9 (4-Feb-2014)
Filesystem at /dev/mapper/Encrypted-Home-LVM is mounted on /home; on-line resizing required
old_desc_blocks = 1, new_desc_blocks = 1
The filesystem on /dev/mapper/Encrypted-Home-LVM is now 523776 blocks long.
```

* **Status of /home filesystem after operation**

```console
root@server:~# df -h | grep home
/dev/mapper/Encrypted-Home-LVM  2.0G  1.6M  1.9G   1% /home
```

### Decrypt LVM from a USB stick

Here you will find a script to extract the key files from a USB stick. Is important to get the UUID for the partition on which the key is stored. This script can be located on /boot partition which is mounted at a very early process when the server starts.

```bash
#!/bin/bash
# Ariel Vasquez v1.0
# Add the driver to recognize the usb storage
if [ "`lsmod | egrep usb_storage`" = "" ]; then
modprobe usb_storage >/dev/null 2>&1
sleep 3
fi

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# First detect if the USB is present
if [ "`lsusb | egrep 'PNY'`" = "" ]; then
exit 0
fi

# Unmount it if it is already mounted
if [ "`mount | egrep '\/dev\/sdb1'`" != "" ]; then
umount /dev/sdb1 >/dev/null 2>&1
fi

# Detects if the directory is present or not
if [ -d "/boot/tmp/key" ]; then
rm -rf /boot/tmp/key >/dev/null 2>&1
fi

mkdir /boot/tmp/key >/dev/null 2>&1

# Mounting
mount -t vfat -o ro,umask=077 UUID="F8AC-8EEF" /boot/tmp/key >/dev/null 2>&1
cat /boot/tmp/key/.key/keyfile

#echo "Done it"
umount /boot/tmp/key >/dev/null 2>&1
rm -rf /boot/tmp/key >/dev/null 2>&1
```
