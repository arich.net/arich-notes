# Tunnel over HTTP

## Table of Content

---
* [1 Table of Content](#table-of-content)
* [2 Introduction](#introduction)
* [3 Warning!](#warning)
* [4 Step by step](#step-by-step)
* [5 HTC over Java](#htc-over-java)
* [6 References](#references)
---

## Introduction

Very often we find ourselves connected to isolated networks behind very restrictive firewalls or security controls, those networks usually allow only for web navigation to Internet, this communication will be filtered not only based on the intrinsic TCP port 80 but also with level 7 filters to allow only HTTP applications, on this situation is almost impossible to navigate using other ports, protocols or applications like SSH or FTP.

By exploring which options exist, I’ve found one which is the main topic of this article. The objective is to encapsulate other ports or protocols within HTTP, on this example I will use the protocol SSH in the default TCP-22 port.

![Tunnel over HTTP diagram](images/TunnelOverHttp1.png "Tunnel over HTTP diagram")

For this purpose, we will use a development created by Lars Brinkhoff called httptunnel. We will show as well as the functionality of this tool, a practical example in Java that could be used for any specific development that we might face.

For this specific post we have worked within Debian Jessie operating system and we have used the Oracle Java version 1.8, it does not mean that will not work for the rest, in fact this example should be compatible with the vast linux operating systems versions in the market or even within Microsoft Windows.

## Warning!

The present post has been written merely for educational purposes, is just an example to use HTTP as an encapsulation method to enable communications to other well-known ports and protocols, taking the advantage that HTTP is widely open and available. In this sense we are NOT responsible for the usage of this information with or without malicious intentions; we are neither responsible for any potential implementation and affectation to ANY production system, it has to be clear that this tool may potentially open back doors that could be misused if they are not well protected. Be much careful about the place you will choose to test it!!!!!

## Step by step

First we install `httptunnel`, it would be possible through package managers like APT or YUM, but for this post we will use and compile the latest development in GITHUB, we will work within a separate folder structure and would be best to also use a different user account rather than root. In the following example we have cloned `httptunnel` from GitHub on a separate folder named tunnel, in parallel we have created another directory named util which will hold all the compiled binaries.

In order to configure and compile `httptunnel` sources, it will be needed `autoconf`. Autoconf will look for `configure.ac` or `configure.in` and dynamically create the configure script.

```console
httptunnel@server:~# cd $HOME
httptunnel@server:~# mkdir tunnel
httptunnel@server:~# cd tunnel
httptunnel@server:~# git clone https://github.com/larsbrinkhoff/httptunnel.git
httptunnel@server:~# mkdir util
httptunnel@server:~# cd httptunnel
httptunnel@server:~# autoreconf --install
httptunnel@server:~# ./configure --prefix=$HOME/tunnel/util --enable-debug
httptunnel@server:~# make
httptunnel@server:~# make install
httptunnel@server:~# cd ../util/bin
```

Let’s explore the options we have with the compiled binaries, this is a client-server application and it has to be installed in both ends.

The options we have from `hts`, which is the server side component, are the following ones:

```console
httptunnel@server:~# ./hts --help
Usage: ./hts [OPTION]... [HOST:][PORT]
Listen for incoming httptunnel connections at PORT (default port is 8888).
When a connection is made, I/O is redirected to the destination specified
by the --device, --forward-port or --stdin-stdout switch.
 
  -c, --content-length BYTES     use HTTP PUT requests of BYTES size
                                 (k, M, and G postfixes recognized)
  -d, --device DEVICE            use DEVICE for input and output
  -D, --debug [LEVEL]            enable debug mode
  -F, --forward-port HOST:PORT   connect to PORT at HOST and use it for 
                                 input and output
  -h, --help                     display this help and exit
  -k, --keep-alive SECONDS       send keepalive bytes every SECONDS seconds
                                 (default is 5)
  -l, --logfile FILE             specify logfile for debug output
  -M, --max-connection-age SEC   maximum time a connection will stay
                                 open is SEC seconds (default is 300)
  -r, --chroot ROOT              change root to ROOT
  -s, --stdin-stdout             use stdin/stdout for communication
                                 (implies --no-daemon)
  -S, --strict-content-length    always write Content-Length bytes in requests
  -u, --user USER                change user to USER
  -V, --version                  output version information and exit
  -w, --no-daemon                don't fork into the background
  -p, --pid-file LOCATION        write a PID file to LOCATION
 
Report bugs to bug-httptunnel@gnu.org.
```

The options we have from `htc`, which is the client side component, are the following ones:

```console
httptunnel@client:~# ./htc --help 
Usage: ./htc [OPTION]... HOST[:PORT]
Set up an httptunnel connection to PORT at HOST (default port is 8888).
When a connection is made, I/O is redirected from the source specified
by the --device, --forward-port or --stdin-stdout switch to the tunnel.
 
  -A, --proxy-authorization USER:PASSWORD  proxy authorization
  -z, --proxy-authorization-file FILE      proxy authorization file
  -B, --proxy-buffer-size BYTES  assume a proxy buffer size of BYTES bytes
                                 (k, M, and G postfixes recognized)
  -c, --content-length BYTES     use HTTP PUT requests of BYTES size
                                 (k, M, and G postfixes recognized)
  -d, --device DEVICE            use DEVICE for input and output
  -D, --debug [LEVEL]            enable debugging mode
  -F, --forward-port PORT        use TCP port PORT for input and output
  -h, --help                     display this help and exit
  -k, --keep-alive SECONDS       send keepalive bytes every SECONDS seconds
                                 (default is 5)
  -l, --logfile FILE             specify file for debugging output
  -M, --max-connection-age SEC   maximum time a connection will stay
                                 open is SEC seconds (default is 300)
  -P, --proxy HOSTNAME[:PORT]    use a HTTP proxy (default port is 8080)
  -s, --stdin-stdout             use stdin/stdout for communication
                                 (implies --no-daemon)
  -S, --strict-content-length    always write Content-Length bytes in requests
  -T, --timeout TIME             timeout, in milliseconds, before sending
                                 padding to a buffering proxy
  -U, --user-agent STRING        specify User-Agent value in HTTP requests
  -V, --version                  output version information and exit
  -w, --no-daemon                don't fork into the background
 
Report bugs to bug-httptunnel@gnu.org.
```

That’s it!, for the present example we will execute the server component to listen on the port 8888, we will redirect the communications to the port 22 of the localhost IP address. On the client side we will open locally the TP port 22222 which will forward all communications through the HTTP tunnel, on this example we have configured the proxy address on the client to make it more “real”.

*SERVER*

```console
httptunnel@server:~#./hts -D 2 -l $HOME/logs/httptunnel.log -F 127.0.0.1:22 8888
```

*CLIENT*

```console
httptunnel@client:~#./htc -P 192.168.1.254:3128 -D 1 -l $HOME/logs/httptunnel.log/logs/httptunnel.log \
                          -F 22222 192.168.1.2:8888
httptunnel@client:~#sudo netstat -tulpn | egrep 2222
tcp        0      0 0.0.0.0:22222           0.0.0.0:*               LISTEN      17904/htc
```

On this example we can see that `htc` is listening locally on the port 22222, which means that the tunnel have opened correctly. To test it we use the normal SSH client to start a connection on the local port opened by htc, what will happen is that `htc` will redirect that traffic through the port 8888, in the other end `hts` will decode that traffic and forward it to the SSH server that is normally listening on TCP-22. You have also a small capture of the traffic which is showing that this SSH communication is actually being encapsulated inside an HTTP POST method.

```console
httptunnel@client:~#ssh -p22222 arich@127.0.0.1
The authenticity of host '[127.0.0.1]:22222 ([127.0.0.1]:22222)' can't be established.
RSA key fingerprint is bc:d9:15:4d:90:b5:17:85:1a:c9:ba:4d:0c:79:51:ee.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '[127.0.0.1]:22222' (RSA) to the list of known hosts.
Debian GNU/Linux 8 \n \l
 
arich@127.0.0.1's password:
```

*TRAFFIC CAPTURE*

```console
19:50:22.085078 IP 192.168.1.254.54751 >  192.168.1.2.8888: Flags [P.], seq 1:215, ack 1, win 457, options [nop,nop,TS val 36525218 ecr 202138253], length 214
        0x0000:  8c89 a52d 9e5d 8019 3468 bf19 0800 4500  ...-.]..4h....E.
        0x0010:  010a 7978 4000 4006 3c25 c0a8 01fe c0a8  ..yx@.@.<%......
        0x0020:  0102 d5df 22b8 9380 2dd7 2b3a 4226 8018  ...."...-.+:B&..
        0x0030:  01c9 7cd0 0000 0101 080a 022d 54a2 0c0c  ..|........-T...
        0x0040:  628d 504f 5354 202f 696e 6465 782e 6874  b.POST./index.ht
        0x0050:  6d6c 3f63 7261 703d 3134 3536 3432 3632  ml?crap=14564262
        0x0060:  3231 2048 5454 502f 312e 310d 0a48 6f73  21.HTTP/1.1..Hos
        0x0070:  743a 2031 3932 2e31 3638 2e31 2e32 3a38  t:.192.168.1.2:8
        0x0080:  3838 380d 0a43 6f6e 7465 6e74 2d4c 656e  888..Content-Len
        0x0090:  6774 683a 2031 3032 3430 300d 0a56 6961  gth:.102400..Via
        0x00a0:  3a20 312e 3120 696e 7465 726e 616c 2d72  :.1.1.internal-r
        0x00b0:  6f75 7465 7220 2873 7175 6964 2f33 2e34  outer.(squid/3.4
        0x00c0:  2e38 290d 0a58 2d46 6f72 7761 7264 6564  .8)..X-Forwarded
        0x00d0:  2d46 6f72 3a20 3139 322e 3136 382e 312e  -For:.192.168.1.
        0x00e0:  3132 0d0a 4361 6368 652d 436f 6e74 726f  12..Cache-Contro
        0x00f0:  6c3a 206d 6178 2d61 6765 3d30 0d0a 436f  l:.max-age=0..Co
        0x0100:  6e6e 6563 7469 6f6e 3a20 6b65 6570 2d61  nnection:.keep-a
        0x0110:  6c69 7665 0d0a 0d0a                      live....
```

With this small test we have confirmed that the encapsulation is actually working but we still need to test it with some Java code which will be the objective of the following chapter.

## HTC over Java

In this chapter we will see an example about the implementation of HTC on Java using a development done by *jcraft*. The source code can be downloaded from its web site or also via GITHUB which is always the preferable method.

For this example we will use a separate directory named `jhttptunnel` and inside three directories: `src`, `lib` and `bin`. Within lib we copy the JAR file compiled and packaged from the source code we have downloaded. During this post I will not explain how to do it, but feel free to comment it and I could detail also that part. We recommend you to run all the steps with a different user than root, but this is up to you anyway.

```console
httptunnel@client:~# mkdir jhttptunnel
httptunnel@client:~# cd jhttptunnel
httptunnel@client:~# mkdir src
httptunnel@client:~# mkdir lib
httptunnel@client:~# mkdir bin
httptunnel@client:~# cp /some/file/location/JHTTPTunnel.jar lib/
```

Inside the directory `src` we create the file `JHTC.java` with the following content:

```java
/** 
        This program is licensed under GNU General Public Licenses
        Copyright (c) arich-net.com
*/

import com.jcraft.jhttptunnel.*;
import java.io.*;
import java.net.*;

public class JHTC {

   public static String hts_server = "127.0.0.1";
   public static int hts_port = 8888;
   public static String local_address = "127.0.0.1";
   public static int local_port = 22222;

   public static void main (String[] args) {

      try {
         ServerSocket ss = new ServerSocket(local_port);
         ss.setReuseAddress(true);
         Socket socket=ss.accept();
         socket.setTcpNoDelay(true);

         System.out.println("Opening local port: " + local_port);

         final InputStream sin=socket.getInputStream();
         final OutputStream sout=socket.getOutputStream();

         final JHttpTunnelClient jhtc=new JHttpTunnelClient(hts_server, hts_port);

         jhtc.setInBound(new InBoundSocket());
         jhtc.setOutBound(new OutBoundSocket());

         jhtc.connect();

         final InputStream jin=jhtc.getInputStream();
         final OutputStream jout=jhtc.getOutputStream();

         Runnable runnable = new Runnable(){
           public void run(){
              byte[] tmp=new byte[1024];
              try {
                 while(true) {
                    int i=jin.read(tmp);
                    if(i>0) {
                       sout.write(tmp, 0, i);
                       continue;
                    }
                    break;
                 }
              }
              catch(Exception e) { }
              try {
                 sin.close();
                 jin.close();
                 jhtc.close();
              }
              catch(Exception e){ }
           }
         };
         (new Thread(runnable)).start();
         byte[] tmp=new byte[1024];
         try {
            while(true) {
               int i=sin.read(tmp);
               System.out.println("i="+i+" "+jout);
               if(i>0) {
                  jout.write(tmp, 0, i);
                  continue;
               }
               break;
            }
         }
         catch(Exception e) { }
      }
      catch (Exception e) { }
   }
}
```

The above development has to be compiled before use. With the following script it can be compiled leaving all executable classes under `bin` directory.

```bash
#!/bin/bash
CLASSPATH=`find . -name '*.jar' -print | sed -e ':a;N;$!ba;s/\n/:/g'`
echo $CLASSPATH
javac -cp $CLASSPATH -d bin src/*.java
```

By executing it as follows, we will notice that the process java will be listening on TCP-22222, and using the same test with SSH as during the previous chapter, we will realize that it works and it prints some connection details.

```console
httptunnel@client:~# CLASSPATH=./bin:./lib/JHTTPTunnel.jar java JHTC          
Opening local port: 22222
i=39 com.jcraft.jhttptunnel.JHttpTunnelClient$2@2503dbd3
i=1024 com.jcraft.jhttptunnel.JHttpTunnelClient$2@2503dbd3
i=944 com.jcraft.jhttptunnel.JHttpTunnelClient$2@2503dbd3
i=48 com.jcraft.jhttptunnel.JHttpTunnelClient$2@2503dbd3
```

## References

* [http://www.jcraft.com/jhttptunnel/](http://www.jcraft.com/jhttptunnel/){:target="_blank"}
* [https://github.com/moontide/JavaHTTPTunnel](https://github.com/moontide/JavaHTTPTunnel){:target="_blank"}
* [http://www.nocrew.org/software/httptunnel.html](http://www.nocrew.org/software/httptunnel.html){:target="_blank"}
