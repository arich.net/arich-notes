# SSH tunnel and iptables

## Table of Contents

---
* [1 Table of Contents](#table-of-contents)
* [2 Introduction](#introduction)
    * [2.1 SSH Reverse Tunnel](#ssh-reverse-tunnel)
    * [2.2 iptables redirection to localhost](#iptables-redirection-to-localhost)

---

## Introduction

On this post I will explain how to enable different communication paths taking the advantage of SSH Reverse Tunnel and `iptables` NAT techniques.

Lets pretend that we want to enable web communications to a “FOO CLIENT” behind a firewall that does not have HTTP port explicitly allowed to the “WEB SERVER” as shown in the figure below.

![SSH tunnel diagram](images/SSHRevTunnel1.png "SSH tunnel diagram")

On the proposed diagram we have a “FOO CLIENT” with no direct access to a “WEB SERVER”, we assume that mentioned “FOO CLIENT” has access to the port 80 on “SERVER B”; in the other hand we have a “SERVER A” with access to the “WEB SERVER” and also with SSH access to “SERVER B”.

The objective is to allow connections from “FOO CLIENT” to “WEB SERVER” using SSH Tunnel Techniques and `iptables`.

---
**Disclaimer**: This article have the only educational purpose to show one practical example of SSH reverse tunnel, be careful with this kind of techniques that could be used to bypass security restrictions on which case we do not hold any responsibility.

---

### SSH Reverse Tunnel

First we open an SSH Reverse Tunnel between “SERVER A” and “SERVER B”, the account we have used is a non-privileged one and for that reason we choose a port above 1024, lets pick 18080 for instance.

```console
user@server-a:~$ ssh -R 127.0.0.1:18080:web-server:80 user@server-b
```

After a successful authentication the `ssh` daemon on “SERVER B” should be listening on port 18080 localhost IP address, We can make sure the port is correctly opened on the destination using the following commands.

```console
user@server-b:~$ sudo netstat -tulpn | egrep 18080
tcp        0      0 127.0.0.1:18080         0.0.0.0:*      LISTEN      9659/4
user@server-b:~$ ps aux | egrep 9659 | egrep -v grep
user     9659  0.0  0.1  11328  3768 ?        S    Mar19   0:00 sshd: user@pts/4
```

At this point we have “SERVER B” listening on the port 18080 bonded to the localhost interface.

---
**Note**: In this case we assume that SSH is forcing the socket to listen on the loopback interface, this can be changed by setting GatewayPorts to Yes on `sshd_config`, by doing that we can forget about the next part.

---

### iptables redirection to localhost

Now we need to prepare `iptables` to get the incoming traffic coming to “SERVER B” on port 80 and redirect it to localhost, the first thing we do is allow the kernel on a specific interface to route traffic to loopback interfaces, that setting is disabled by default.

```console
user@server-b:~$ sudo sysctl -w net.ipv4.conf.eth0.route_localnet=1
```

Now we use `iptables` “nat” table and on the PREROUTING chain and we MARK incoming packets that comes to TCP-80, from "foo client" with “0x1”.

```console
user@server-b:~$ export foo_ip=192.168.X.X
user@server-b:~$ sudo iptables -t nat --append PREROUTING \
                               -s $foo_ip -p tcp --dport 80 \
                               -j MARK --set-mark 1
```

The next step is to change the destination IP (DNAT) of the incoming packet on port 80 to localhost port 18080.

```console
user@server-b:~$ sudo iptables -t nat --append PREROUTING \
                               -s $foo_ip -p tcp --dport 80 \
                               -j DNAT --to-destination 127.0.0.1:18080
```

Finally on the POSTROUTING chain when the packet with MARK “0x1” is found we DNAT it to any available localhost IP address (i.e. 127.0.0.10)

```console
user@server-b:~$ sudo iptables -t nat --append POSTROUTING \
                               -m mark --mark 2 -j SNAT \
                               --to-source 127.0.0.10
```

That's all, enjoy!