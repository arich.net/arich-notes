# Manage CISCO devices using SNMP

## Table of Content

---
* [1 Table of Content](#table-of-content)
* [2 Introduction](#introduction)
* [3 Main Steps](#main-steps)
    * [3.1 Install gem snmp module](#install-gem-snmp-module)
    * [3.2 Check imported MIBs on Ruby Framework](#check-imported-mibs-on-ruby-framework)
    * [3.3 Import CISCO MIBs on Ruby repositories](#import-cisco-mibs-on-ruby-repositories)
    * [3.4 Ruby SNMP implementations example](#ruby-snmp-implementations-example)

---

## Introduction

This post will go through some tests I did trying to automate some VLAN deployment on CISCO switches, there is some information on the NET, one of the most useful one you can find it here. Basically the main objective is by using Ruby been able to perform some basic configuration on CISCO switches. The main requisites in order to understand this post are the following:

* Understand SNMP protocol.
* Understand structure of MIBs.
* Some development skills :smile:
* Linux system with ruby and gems.

## Main Steps

### Install gem snmp module

During this post we will be using snmp gem library, please use the information about this class [here](http://www.rubydoc.info/github/hallidave/ruby-snmp/SNMP)

```console
~# sudo gem install snmp
```

### Check imported MIBs on Ruby Framework

The following utility will be useful in order to check which MIBs has been imported with this package.

```ruby
#!/usr/local/bin/ruby
require 'snmp'
include SNMP

if MIB.import_supported? then
        puts "Import is supported.  Available MIBs include:"
        mib_list = MIB.list_imported
        puts mib_list
end
```

### Import CISCO MIBs on Ruby repositories

In order to import CISCO MIBs is necessary `libsmi` available [here](https://www.ibr.cs.tu-bs.de/projects/libsmi/). SMI will be used to compile and export MIBs to YAML, this library can be installed using Ubuntu repositories.

```console
~# sudo dpkg -l | egrep libsmi
ii libsmi2ldbl 0.4.8+dfsg2-4build1 library to access SMI MIB information
```

SNMP MIBs has dependencies on other MIBs, libsmi will use the environment variable SMIPATH in which you should place for instance IETF MIBs along with other dependencies, all CISCO MIBs dependencies are documented on line.

```console
~# echo $SMIPATH
/home/avasquez/Documents/Scripts/SNMP/mibs:/usr/share/mibs/netsnmp
```

For our tests the following MIBs will be required.

```console
# CISCO-VTP-MIB
# ftp://ftp.cisco.com/pub/mibs/v2/CISCO-VTP-MIB.my
 
# CISCO-VLAN-MEMBERSHIP-MIB
# ftp://ftp.cisco.com/pub/mibs/v2/CISCO-VLAN-MEMBERSHIP-MIB.my
 
# BRIDGE-MIB
# ftp://ftp.cisco.com/pub/mibs/v1/BRIDGE-MIB.my
 
# IF-MIB
# ftp://ftp.cisco.com/pub/mibs/v2/IF-MIB.my
 
# CISCO-PRIVATE-VLAN-MIB
# ftp://ftp.cisco.com/pub/mibs/v2/CISCO-PRIVATE-VLAN-MIB.my
```

Before importing any MIB is useful to “debug” if we have all the requirements, this is something that can de bone executing the following command, which will output some warnings or errors when compiling and exporting mentioned MIBs.

```console
~# smidump -l 10 -f python -k CISCO-VLAN-MEMBERSHIP-MIB.my 2&>1 | more
```

Sometimes MIBs are on SMIv1 format and we will have to convert them file as SMIv2 compatible.

```console
~# smidump -l 10 -f smiv2 -k CISCO-VLAN-MEMBERSHIP-MIB.my > smi2/CISCO-VLAN-MEMBERSHIP-MIB.my
```

After being sure that the `libsmi` parses the MIBs successfully we code the following ruby script to actually import the MIB. In this case I am importing and generating the YAML file on `~/mibs/ruby`.

```ruby
#!/usr/bin/ruby

require 'snmp'
require 'pp'
include SNMP

if ARGV.empty? then
  puts "Please pass the MIB you want to import"
  exit 1
end

if MIB.import_supported? then
  puts "Import module #{ARGV[0]}"
  MIB.import_module(ARGV[0], "~/mibs/ruby/")
end

puts "Done."
```

The successful import of the MIB will generate a YAML file, which will have to be copied to GEM mibs directory.

```console
~# sudo mv CISCO-VLAN-MEMBERSHIP-MIB.yaml \
        /var/lib/gems/1.9.1/gems/snmp-1.2.0/data/ruby/snmp/mibs/
```

### Ruby SNMP implementations example

Find below an example of the usage on some SNMP WALK, GET and SET methods using CISCO MIBs.

```ruby
#!/usr/bin/ruby

require 'snmp'
require 'pp'
#include SNMP

manager = SNMP::Manager.new(:Host => 'switch_hostname',
                            :Community => 'private',
                            :Version => :SNMPv2c)

manager.load_module("CISCO-VLAN-MEMBERSHIP-MIB")
mib = SNMP::MIB.new
mib.load_module("CISCO-VLAN-MEMBERSHIP-MIB")

# Extract interfaces names of a switch
manager.walk("ifDescr") do |result|
    puts "#{result.value} #{result.name}"
end

# Get VMVLAN of interface 31
results = manager.get(['vmVlan.31'])

results.each_varbind do |result|
    pp result
    puts "#{result.value} #{result.name}"
end

# Set VMVLAN value on interface 31
vlan_oid = mib.oid("vmVlan.31")
vlan_id = SNMP::Integer.new(7)
set_variable = SNMP::VarBind.new(vlan_oid, vlan_id)
result = manager.set(set_variable)

if result.error_status == :noError then
   puts "VLAN stored successfully"
else
   puts "VLAN NOT stored successfully"
end

#pp manager

manager.close

puts "Done..."
```
