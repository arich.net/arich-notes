# Riverbed LB issues importing certs

## Table of Content

---
* [1 Table of Content](#table-of-content)
* [2 Introduction](#introduction)
* [3 Problem](#problem)
* [4 Solution](#solution)
* [5 References](#references)
---

## Introduction

This post does not explain a step-by-step process on importing certificates on riverbed, the main intention is to explain one little challenge we have faced when importing a certificate for SSL Decryption. This post will not cover the theory behind rsa keys and x509 certificates.

## Problem

Lets assume that the knowledge on importing certificates on riverbed is well known, in fact is not a difficult task using the web interface. The problem arise with some keys without any reasonable explanation about the root cause, the conflict happens when importing the private key on the appliance gives the following error

![Error in Riverbed](images/ImportingCertsIssue.png "Error in Riverbed Importing Certificates")

The problem is that everything seemed to be fine with the key, it was actually on PEM format and `openssl` was not giving any clue about the issue. The tests I have done were verifying that the key was actually on PEM format, and also that the key correspond to the certificate.

```console
user@workstation:~# openssl rsa -in badkeytest.key -inform PEM -check -noout
RSA key ok
user@workstation:~# openssl x509 -in certtest.crt -inform PEM -noout && echo $?
0
user@workstation:~# openssl rsa -in badkeytest.key -inform PEM -noout -modulus | openssl md5
(stdin)= 12cf2c78f4d646c2466aaab76df3adc1
user@workstation:~# openssl x509 -in certtest.crt -inform PEM -noout -modulus | openssl md5
(stdin)= 12cf2c78f4d646c2466aaab76df3adc1
```

Maybe something much better would be to test inside the appliance itself which also done, but without luck it was also giving the bad sensation of being correct.

```console
user@workstation:~# $ZEUSHOME/admin/bin/cert -in /tmp/certtest.crt -key /tmp/badkeytest.key --check
private and public key are a valid pair
```

## Solution

The fact that the private key was apparently OK gave the impression that it has a different encoding settings in a way that it was not being accepted by riverbed, maybe due the pair was generated on a Windows device and that `riverbed` is a Linux appliance. So the first thing I have done is export it on the same PEM format, weird but was the only valid solution I could find. In fact after re-exporting the key back again both files differ, but the resultant modulus are the same.

```console
user@workstation:~#  openssl rsa -in badkeytest.key -inform PEM -out goodkeytest.key -outform PEM
writing RSA key
user@workstation:~#  diff -q badkeytest.key goodkeytest.key
Files badkeytest.key and goodkeytest.key differ
user@workstation:~# openssl rsa -in goodkeytest.key -inform PEM -modulus -noout | openssl md5
(stdin)= 12cf2c78f4d646c2466aaab76df3adc1
user@workstation:~# openssl rsa -in badkeytest.key -inform PEM -modulus -noout | openssl md5
(stdin)= 12cf2c78f4d646c2466aaab76df3adc1
```

In fact if we analyse in depth both resultant rsa keys using the following command (goodkey and badkey), we will realize that both actually differ on some bytes!, the explanation of why maybe on future analysis or investigations.

```console
user@workstation:~# openssl base64 -d -in badkeytest.key | \
                    hexdump | awk '{$1=""; print $0}' | \
                    sed ':a;N;$!ba;s/\n / /g'
```

After re-exporting the key and then try to import the certificate back again on riverbed the result was successful.

![Solution Importing Certs](images/ImportingCertsIssue2.png "Solution Importing Certificates")

![Solution Importing Certs](images/ImportingCertsIssue3.png "Solution Importing Certificates")

Hope this post can help deal with this kind of issues.

## References

* [https://splash.riverbed.com/thread/5646](https://splash.riverbed.com/thread/5646){:target="_blank"}